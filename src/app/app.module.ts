import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateComponent } from './pages/employee/create/create.component';
import { EditComponent } from './pages/employee/edit/edit.component';
import { IndexComponent } from './pages/employee/index/index.component';

@NgModule({
  declarations: [AppComponent, CreateComponent, EditComponent, IndexComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from 'src/app/model/Employee';
import { PaginatedQuery } from 'src/app/model/PaginationQuery';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class EmployeeServiceService {
  protected url1 = 'http://18.224.53.106:8080';
  constructor(private http: HttpClient) {}

  getEmployee(): Observable<any> {
    
    return this.http.get(`${this.url1}/Employee`);
  }
  // getEmployee2(data: PaginatedQuery) {
  //   return this.http.get<Employee[]>(`${this.url1}/Employee`, {
  //     params: data as any,
  //   });
  // }
}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from './model/Employee';
import { EmployeeServiceService } from './pages/employee/employee-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'my-app';
  employees: Employee[] = [];

  constructor(
    public employeeService: EmployeeServiceService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.fetchEmployee();
  }
  // { pageNumber: 1, PageSize: 10 }
  fetchEmployee() {
    debugger;
    this.employeeService.getEmployee().subscribe((data) => {
      this.employees = data;
      console.log('employee', data);
    });
  }
}
